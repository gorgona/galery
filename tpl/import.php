<?php

require_once 'parts/header.php';
?>

    <div class="col-lg-6 col-lg-offset-3">


        <div class="panel panel-info" style="margin-top:20px;">
            <div class="panel-heading">
                <h2 style="margin:0;" class="ng-binding">Import Images</h2>
            </div>
            <div class="panel-body">
                <form enctype="multipart/form-data" method="POST">

                    <div class="form-group">
                        <label>Latitude</label>
                        <input name="latitude" type="text" class="form-control" value="49.991639" />
                    </div>

                    <div class="form-group">
                        <label>Longitude</label>
                        <input name="longitude" type="text" class="form-control" value="36.232417" />
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-primary form-control" value="Import" />
                    </div>

                </form>
            </div>
        </div>

    </div>



<?php

require_once 'parts/footer.php';

?>