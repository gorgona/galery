<?php
/**
 * Created by PhpStorm.
 * User: Елена
 * Date: 16.10.2015
 * Time: 21:56
 */
//Начинаем делать класс GalleryDB, он должен быть синглтоном, в конструкторе должно происходить подключение к БД ,
// в конструктор должны передаватся host, dbname, login, password.
// Первый метод который нужно реализовать checkLogin, который будет проверять логин и пароль в БД.
//40bd001563085fc35165329ea1ff5c5ecbdbbeef  - 123
Class GaleryDB
{
    private $connect;
    protected static $_instance;

    private function __construct($host, $dbname, $login,$password)
    {
        $this->connect = new PDO("mysql:host=$host; dbname=$dbname","$login","$password");
        //$this->connect =new PDO("mysql:host=localhost; dbname=galery_",'root','');
}

    public static function getInstance($host,$dbname,$login,$password)
    {
        if (null === self::$_instance) {
            self::$_instance = new self($host,$dbname,$login,$password);
        }
        return self::$_instance;
    }

    public function checkLogin($name,$pass)
    {
        $st = $this->connect->prepare('select id from user where email=:log and password=:pass');
        $st->bindvalue(':log', $name);
                $st->bindValue(':pass',sha1($pass));
                $st->execute();
               $data = $st->Fetch(PDO::FETCH_ASSOC);

             if (isset($data['id'])) {
                 $_SESSION['id'] = $data['id'];
                 echo 'check login!!!';
                 return ($data['id']);
             }
       }

   public function createAlbum($name,$discription){
    $st=$this->connect->prepare('insert into album(name,discription,user_id) values(:name,:discr,:user_id)');
       $st->bindValue(':name',$name);
       $st->bindValue(':discr',$discription);
       $st->bindValue(':user_id',$_SESSION['id']);
       $st->execute();
       if ($st->rowCount()<1){
           echo 'Insert album error!';
       }
       if ($st->rowCount()==1){
           echo 'Done!';
       }
}
}